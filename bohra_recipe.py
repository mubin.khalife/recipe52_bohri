from requests import get
from bs4 import BeautifulSoup
from pandas import DataFrame
from collections import OrderedDict
from tkinter import (Tk, Frame, Label, Button, messagebox, DISABLED, NORMAL)
from inspect import getfile, currentframe
from os import path
# from sys import platform, exc_info
from sys import platform
from subprocess import call
from _thread import start_new_thread
from queue import Queue
# import traceback


completion_queue = Queue()


class BohraRecipeScrap:
    def __init__(self, root, baseUrl):
        self.recipeLinks = []
        self.failedLinks = []
        self.totalProcessedLinks = []
        self.lst_cumulative_data = []
        self.window = root
        self.baseUrl = baseUrl
        self.window.bind("<Control-q>", lambda _event: self.window.destroy())
        self.window.bind("<Control-Q>", lambda _event: self.window.destroy())

        self.frm = Frame(self.window)
        self.frm.grid(row=0, column=0)

        self.lblTopMessage = Label(self.frm,
                                   text=self.__str__(),
                                   justify="center", wraplength=300)
        self.lblTopMessage.grid(
            row=2, column=0, columnspan=2, pady=(10, 10), padx=(10, 10))

        self.btnSearch = Button(self.frm, text="Press to fetch Recipes",
                                justify="center",
                                command=lambda:
                                self.fetchRecipes())
        self.btnSearch.grid(row=3,  column=0, columnspan=2,
                            pady=(10, 10), padx=(10, 10))

        self.Lab = Label(self.frm)
        self.Lab.grid(row=5,  column=0, columnspan=2,
                      pady=(10, 10), padx=(10, 10))

        self.lblTopMessage.grid_rowconfigure(2, weight=1)
        self.btnSearch.grid_columnconfigure(3, weight=1)
        self.Lab.grid_columnconfigure(5, weight=1)

    def fetchRecipes(self):
        self.btnSearch.configure(state=DISABLED)
        self.Lab["text"] = "Fetching total pages...Please wait......."
        start_new_thread(self.getTotalPages, ())

    def __str__(self):
        return "Web Scrap application to fetch Bohra Recipes\n"

    def getTotalPages(self):
        try:
            r = get(self.baseUrl)
            c = r.content

            scrapped_soup = BeautifulSoup(c, "html.parser")
            divPageTitle = scrapped_soup.find_all(
                "div", {"class": "page-title"})
            paginate = (divPageTitle[0].find("p")).text
            total_page = int(paginate[-1])
            # return total_page
            self.Lab.config(text="Total pages fetched...Fetching all links")
            self.frm.update_idletasks()
            start_new_thread(self.fetchLinks, (total_page,))
        except (IndexError, KeyError, KeyboardInterrupt):
            self.Lab.config(text="")
            self.frm.update_idletasks()
            messagebox.showerror(title="Error",
                                 message="Process stopped at " +
                                 "getting total pages")

    def fetchLinks(self, total_page):
        try:
            for i in range(1, total_page+1):
                custom_url = ""
                if i == 1:
                    custom_url = self.baseUrl
                else:
                    custom_url = self.baseUrl+"/page/{}/".format(i)

                r = get(custom_url)
                c = r.content

                scrapped_soup = BeautifulSoup(c, "html.parser")
                postTitles = scrapped_soup.find_all(
                    "h2", {"class": "post-title"})
                for a_tag in postTitles:
                    a = a_tag.find("a", href=True)
                    # print("Page {}".format(i), a.text, a["href"])
                    self.recipeLinks.append(a["href"])

            # start_new_thread(self.fetchData, ())
            # return total_page
            self.Lab.config(
                text="All links fetched...now beginning" +
                "to fetch the required data")
            self.frm.update_idletasks()
            self.fetchData()

            if str(completion_queue.get()).lower() == "all_data":
                df = DataFrame(self.lst_cumulative_data)
                df.to_csv("recipes.csv", index=False)
                self.btnSearch.configure(state=NORMAL)
                optn_selected = messagebox.askyesno("File generated.",
                                                    "Do you want to open"
                                                    " the file?")
                if optn_selected:
                    script_dir = (path.dirname(path.abspath(
                        getfile(currentframe()))))
                    file_name = "recipes.csv"
                    abs_file_path = path.join(script_dir, file_name)
                    opener = "open" if platform == "darwin" else "xdg-open"
                    call([opener, abs_file_path])
                else:
                    self.Lab.config(text="")
                    self.frm.update_idletasks()

        except (IndexError, KeyError, KeyboardInterrupt):
            self.Lab.config(text="")
            self.frm.update_idletasks()
            messagebox.showerror(title="Error",
                                 message="Process stopped at getting" +
                                 "fetching of links")

    def fetchData(self):
        try:
            self.Lab.config(text="Found {} links...".format(
                len(self.recipeLinks)))
            self.frm.update_idletasks()
            for link in self.recipeLinks:
                start_new_thread(self.fetchUrlData, (link,))

        except (IndexError, KeyError, KeyboardInterrupt):
            self.Lab.config(text="")
            self.frm.update_idletasks()
            messagebox.showerror(title="Error",
                                 message="Process stopped at" +
                                 "getting fetching of data")

    def fetchUrlData(self, link):
        try:
            r = get(link)
            c = r.content

            scrapped_soup = BeautifulSoup(c, "html.parser")

            divContent = scrapped_soup.find(
                "div", {"class": "post-content"})
            if divContent is not None:
                d = OrderedDict()
                d["Url"] = link

                postTitle = scrapped_soup.find("h1", {"class": "post-title"})
                if postTitle is not None:
                    d["Name"] = postTitle.text
                else:
                    postTitle = scrapped_soup.find(
                        "h2", {"class": "post-title"})
                    if postTitle is not None:
                        d["Name"] = postTitle.text

                pInfoTag = divContent.find_all("p")
                strInfo = ""
                for info in pInfoTag:
                    strInfo += (((info.text).strip())
                                .replace("\n", "")).replace("\t", "")
                d["Information"] = strInfo
                courseCategoryContainer = divContent.find(
                    "span", {"class": "wprm-recipe-course"})
                if courseCategoryContainer is not None:
                    d["Course"] = (courseCategoryContainer.text).strip()
                cuisineCategoryContainer = divContent.find(
                    "span", {"class": "wprm-recipe-cuisine"})
                if cuisineCategoryContainer is not None:
                    d["Cuisine"] = (cuisineCategoryContainer.text).strip()
                servingCategoryContainer = divContent.find(
                    "span", {"class": "wprm-recipe-servings"})
                if servingCategoryContainer is not None:
                    d["Serving"] = (servingCategoryContainer.text).strip()
                postCategories = scrapped_soup.find(
                    "p", {"class": "post-categories"})
                if postCategories is not None:
                    catergoryTag = postCategories.find_all("a")
                strLinkText = ""
                for ref in catergoryTag:
                    strLinkText = strLinkText + " " + ref.text

                d["Category"] = strLinkText

                videoFrame = divContent.find(
                    "iframe", {"class", "youtube-player"})
                if videoFrame is not None:
                    d["Video Link"] = videoFrame["src"]
                else:
                    d["Video Link"] = ""

                prepTime = divContent.find(
                    "span", {"class": "wprm-recipe-prep_time"})
                if prepTime is not None:
                    prepTimeUnit = divContent.find(
                        "span", {"class": "wprm-recipe-prep_time-unit"})
                    d["Prep time"] = "{} {}".format(
                        prepTime.text, prepTimeUnit.text)

                    cookTime = divContent.find(
                        "span", {"class": "wprm-recipe-cook_time"})
                    cookTimeUnit = divContent.find(
                        "span", {"class": "wprm-recipe-cook_time-unit"})
                    d["cook time"] = "{} {}".format(
                        cookTime.text, cookTimeUnit.text)

                totalTime = divContent.find(
                    "span", {"class": "wprm-recipe-total_time"})
                if totalTime is not None:
                    totalTimeUnit = divContent.find(
                        "span", {"class": "wprm-recipe-total_time-unit"})
                    d["total time"] = "{} {}".format(
                        totalTime.text, totalTimeUnit.text)

                if courseCategoryContainer is None:
                    strongTags = divContent.find_all("strong")
                    lstSplit = []
                    for tag in strongTags:
                        if "Prep time" in tag.text:
                            lstSplit = tag.text.split("|")
                            break
                    if len(lstSplit) > 0:
                        for item in lstSplit:
                            if "Recipe type" in item:
                                d["Course"] = (item.split(":")[1]).strip()
                            if prepTime is None and "Prep time" in item:
                                d["Prep time"] = (item.split(":")[1]).strip()
                            if "cook time" not in d and ("Cooking time" in
                                                         item or "Cook time"
                                                         in item):
                                d["cook time"] = (item.split(":")[1]).strip()
                            if "Recipe time" in item:
                                d["total time"] = (item.split(":")[1]).strip()
                            if cuisineCategoryContainer is None \
                                    and "Cuisine" in item:
                                d["Cuisine"] = (item.split(":")[1]).strip()
                            if servingCategoryContainer is None \
                                    and "Serving" in item:
                                strServing = (item.split(":")[1]).strip()
                                d["Serving"] = "".join([s for s in
                                                        strServing.split()
                                                        if s.isdigit()])

                ingredients = divContent.find_all(
                    "li", {"class", "wprm-recipe-ingredient"})

                if ingredients is not None and len(ingredients) > 0:
                    lsIngredient = []
                    for ingredient in ingredients:
                        lsIngredient.append(
                            (str(ingredient.text).strip()).replace("\n", ""))

                    d["ingredients"] = "$".join(lsIngredient)
                else:
                    ulClasslessTag = divContent.find("ul", {"class": None})

                    children = ulClasslessTag.findChildren(
                        "li", recursive=False)
                    lsIngredient = []
                    for child in children:
                        lsIngredient.append(
                            (str(child.text).strip()).replace("\n", ""))
                    d["ingredients"] = "$".join(lsIngredient)

                instructions = divContent.find_all(
                    "li", {"class", "wprm-recipe-instruction"})

                if instructions is not None and len(instructions) > 0:
                    lsInstructions = []
                    for instruction in instructions:
                        lsInstructions.append(
                            (str(instruction.text).strip()).replace("\n", ""))

                    d["instructions"] = "$".join(lsInstructions)
                else:
                    olClasslessTag = divContent.find_all("ol", {"class": None})
                    lsIngredient = []
                    for ol in olClasslessTag:
                        children = ol.findChildren("li", recursive=False)
                        # for children in olClasslessTag.find_all("li"):
                        for child in children:
                            lsIngredient.append(
                                (str(child.text).strip()).replace("\n", ""))
                    d["instructions"] = "$".join(lsIngredient)

                images = scrapped_soup.findAll('img')
                for indx, image in enumerate(images):
                    if indx == 0:
                        d["Main Image"] = image['src']
                    else:
                        d["image{}".format(indx)] = image['src']

                self.lst_cumulative_data.append(d)

                if link in self.failedLinks:
                    self.failedLinks.remove(link)

            else:
                # print("Nothing for {} ".format(link))
                self.failedLinks.append(link)

            if not self.failedLinks:
                if len(self.recipeLinks) == len(self.lst_cumulative_data):
                    self.Lab.config(
                        text="All data fetched...process is complete")
                    self.frm.update_idletasks()
                    completion_queue.put("all_data")
                else:
                    self.Lab.config(text="Processed {} out of {} links".format(
                        len(self.lst_cumulative_data), len(self.recipeLinks)))
                    self.frm.update_idletasks()
            else:
                if len(self.recipeLinks) == (len(self.lst_cumulative_data)
                                             + len(self.failedLinks)):
                    for l in self.failedLinks:
                        start_new_thread(self.fetchUrlData, (l,))
                else:
                    self.Lab.config(text="Processed {} out of {} links".format(
                        len(self.lst_cumulative_data) + len(self.failedLinks),
                        len(self.recipeLinks)))
                    self.frm.update_idletasks()

        except (IndexError, KeyError, KeyboardInterrupt, AttributeError):
            # except BaseException as ex:
            # Get current system exception
            # ex_type, ex_value, ex_traceback = exc_info()

            # # Extract unformatter stack traces as tuples
            # trace_back = traceback.extract_tb(ex_traceback)

            # # Format stacktrace
            # stack_trace = list()
            # print("===Error occured====")
            # print(str(ex))
            # print("-------------")
            # print(link)
            # print("===Error occured====")

            # for trace in trace_back:
            #     stack_trace.append("File : %s , Line : %d,
            # Func.Name : %s, Message : %s" % (
            #         trace[0], trace[1], trace[2], trace[3]))

            # print("Exception type : %s " % ex_type.__name__)
            # print("Exception message : %s" % ex_value)
            # print("Stack trace : %s" % stack_trace)

            # print("===Error occured====")
            # print(str(e))
            # print("-------------")
            # print(link)
            # print("===Error occured====")
            self.Lab.config(text="")
            self.frm.update_idletasks()
            messagebox.showerror(
                title="Error", message="Process stopped at" +
                "getting total pages")
            completion_queue.put("err_url_data")
            self.window.destroy()


if __name__ == '__main__':
    try:
        window = Tk()
        window.geometry("500x500")
        # Don't allow resizing along x or y direction
        window.resizable(0, 0)
        window.title("Bohra Recipe Web Scrap")
        scrap = BohraRecipeScrap(
            window, "https://recipe52.com/category/bohra-recipes/")
        window.grid_columnconfigure(0, weight=1)
        window.mainloop()
    except (KeyboardInterrupt):
        pass
